﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Xml;
using System.Reflection;
using Object = UnityEngine.Object;

public class XML_Loader
{
	[MenuItem("Tools/Create Sprites From Selection")]
	public static void LoadXML()
	{
		var selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);

		foreach (var item in selection)
		{
			// Original path example: Assets/2D/Knight/knight_idle.png
			var imagePath = AssetDatabase.GetAssetPath(item);

			var xmlPath = imagePath.Replace("Assets/", "").Replace("png", "xml");
			xmlPath = Path.Combine(Application.dataPath, xmlPath);

			SetSprites(imagePath, xmlPath);
		}

	}

	private static void SetSprites(string imagePath, string xmlPath)
	{
		if (!File.Exists(imagePath))
		{
			Debug.LogError("Image not found");
			return;
		}

		if (!File.Exists(xmlPath))
		{
			Debug.LogError("XML File not found");
			return;
		}

		var texture2D = LoadTexture(imagePath);
		if (texture2D == null)
		{
			Debug.LogError("File is not an Image");
			return;
		}

		var xmlDoc = new XmlDocument();
		xmlDoc.Load(xmlPath);

		var container = new List<SubTexture>();
		foreach (XmlNode node in xmlDoc.GetElementsByTagName("SubTexture"))
		{
			var subTexture = new SubTexture();
			subTexture.AddAttributes(node.Attributes);
			container.Add(subTexture);
		}

		var ti = AssetImporter.GetAtPath(imagePath) as TextureImporter;
		if (ti == null)
		{
			return;
		}

		ti.isReadable = true;
		ti.spriteImportMode = SpriteImportMode.Multiple;

		var smdData = new List<SpriteMetaData>();

		foreach (var subTex in container)
		{
			var smd = new SpriteMetaData();
			smd.name = subTex.Name;
			smd.alignment = (int)SpriteAlignment.Custom;

			var pivotX = (subTex.Width * 0.5f + subTex.FrameX) / subTex.Width;
			var pivotY = 0;

			smd.pivot = new Vector2(pivotX, pivotY);
			smd.rect = new Rect(subTex.X, texture2D.height - subTex.Y - subTex.Height, subTex.Width, subTex.Height);
			smdData.Add(smd);
		}

		ti.spritesheet = smdData.ToArray();
		AssetDatabase.ImportAsset(imagePath, ImportAssetOptions.ForceUpdate);
	}

	private static Texture2D LoadTexture(string path)
	{
		if (File.Exists(path))
		{
			var fileData = File.ReadAllBytes(path);
			var tex2D = new Texture2D(1, 1);
			if (tex2D.LoadImage(fileData))
			{
				return tex2D;
			}
		}

		return null;
	}

	private class SubTexture
	{
		public string Name;
		public int FrameWidth;
		public int FrameHeight;
		public int FrameX;
		public int FrameY;
		public int Height;
		public int Width;
		public int X;
		public int Y;

		public void AddAttributes(XmlAttributeCollection attributes)
		{
			foreach (XmlAttribute attribute in attributes)
			{
				AddAttribute(attribute);					
			}
		}

		private void AddAttribute(XmlAttribute attribute)
		{
			var fields = GetType().GetFields();

			foreach (var field in fields)
			{
				if (string.Equals(attribute.Name, "Name", StringComparison.InvariantCultureIgnoreCase))
				{
					Name = attribute.Value;
					continue;
				}

				if (!string.Equals(attribute.Name, field.Name, StringComparison.InvariantCultureIgnoreCase))
				{
					continue;
				}

				var val = 0;
				if (int.TryParse(attribute.Value, out val))
				{
					field.SetValue(this, val);
				}
			}
		}
	}
}
